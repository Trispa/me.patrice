/* eslint-disable @next/next/no-img-element */

import About from "@/components/About";
import ContactMe from "@/components/ContactMe";
import Header from "@/components/Header";
import Hero from "@/components/Hero";
import Projects from "@/components/Projects";
import Skills from "@/components/Skills";
import WorkExperience from "@/components/WorkExperience";
import Image from "next/image";
import Link from "next/link";
import { Experience, PageInfo, Project, Skill, Social } from "@/typings";
import { fetchPageInfo } from "@/utils/fetchPageInfo";
import { fetchExperiences } from "@/utils/fetchExperiences";
import { fetchSocials } from "@/utils/fetchSocials";
import { fetchSkills } from "@/utils/fetchSkills";
import { fetchProjects } from "@/utils/fetchProjects";
import Articles from "@/components/Articles";

export default async function Home() {
  const socials: Social[] = await fetchSocials();
  const info: PageInfo = await fetchPageInfo();
  const skills: Skill[] = await fetchSkills();
  const projects: Project[] = await fetchProjects();
  const experiences: Experience[] = await fetchExperiences();
  return (
    <div className="bg-[rgb(36,36,36)] text-white h-screen snap-y snap-mandatory overflow-y-scroll z-0 overflow-x-hidden  scrollbar-track-gray-400/20 scrollbar-thumb-[#F7AB0A]/80 scrollbar-thin">
      <Header socials={socials} />
      <section id="hero" className="lg:snap-center">
        <Hero info={info} />
      </section>
      <section id="about" className="lg:snap-center">
        <About info={info} />
      </section>
      <section id="experiences" className="lg:snap-center">
        <WorkExperience experiences={experiences} />
      </section>

      <section id="skills" className="lg:snap-center">
        <Skills skills={skills} />
      </section>
      <section id="projects" className="lg:snap-center">
        <Projects projects={projects} />
      </section>
      <section id="contact" className="lg:snap-center">
        <ContactMe />
      </section>
      <section id="articles" className="lg:snap-center">
        <Articles />
      </section>
      <Link href="#hero">
        <footer className="sticky bottom-5 w-full cursor-pointer  items-center  justify-center flex">
          <div className="h-10 w-10  grayscale hover:grayscale-0 filter">
            <Image src="/image/pic.png" alt="" className="rounded-full" fill />
          </div>
        </footer>
      </Link>
    </div>
  );
}
