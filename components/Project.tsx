"use client";
import React from "react";
import { motion } from "framer-motion";
import { Project } from "@/typings";
import { urlFor } from "@/sanity";
import Image from "next/image";
type Props = {
  index: number;
  project: Project;
  projects: Project[];
};

export default function Project({ index, project, projects }: Props) {
  return (
    <div className="w-screen flex-shrink-0 snap-center flex flex-col space-y-5 items-center justify-center p-10 md:p-44 h-screen">
      <motion.img
        initial={{
          y: -300,
          opacity: 0,
        }}
        transition={{ duration: 1.2 }}
        whileInView={{ opacity: 1, y: 0 }}
        viewport={{ once: true }}
        alt=""
        src={urlFor(project.image).url()}
        className="h-56 w-56 lg:h-72 lg:w-72 xl:w-96 xl:h-96"
      />
      <div className="space-y-10 px-0 md:px-10 max-w-6xl">
        <h4 className="md:text-4xl  text-xl font-semibold text-center">
          <span className="underline decoration-[#F7AB0A]/50">
            Case Study {index + 1} of {projects.length}:
          </span>{" "}
          {project.title}
        </h4>
        <div className="flex items-center space-x-2 justify-center flex-wrap">
          {project.technologies.map((technology) => (
            <div className="relative h-10 w-10" key={technology._id}>
              <Image
                src={urlFor(technology.image).url()}
                alt=""
                fill
                className="rounded-full"
              />
            </div>
          ))}
        </div>
        <p className="text-xs lg:text-lg text-center md:flex-left ">
          {project?.summary}
        </p>
      </div>
    </div>
  );
}
