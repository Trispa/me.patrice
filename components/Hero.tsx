"use client";
import React from "react";
import { Cursor, useTypewriter } from "react-simple-typewriter";
import BackgroundCircles from "./BackgroundCircles";
import Image from "next/image";
import Link from "next/link";
import { PageInfo } from "@/typings";
import { urlFor } from "@/sanity";

type Props = {
  info: PageInfo;
};

export default function Hero({ info }: Props) {
  const [text, count] = useTypewriter({
    words: [
      `Hi, I'm ${info?.name}`,
      "I'm empowering People Through Digital Innovation",
      "Guy-who-loves-coding.tsx",
      "<ButDoNotDrinkCoffee/>",
    ],
    loop: true,
    delaySpeed: 500,
  });
  return (
    <div className="h-screen flex flex-col space-y-8 items-center justify-center text-center overflow-hidden">
      <BackgroundCircles />
      <div className="relative rounded-full h-32 w-32 mx-auto object-cover">
        <Image
          src={urlFor(info?.heroImage).url()}
          fill
          alt="pic"
          className="rounded-full"
        />
      </div>
      <div className="z-20">
        <h2 className="text-sm uppercase text-gray-500 pb-2 tracking-[15px]">
          {info.role}
        </h2>
        <h1 className="text-lg lg:text-3xl font-semibold px-10">
          <span className="mr-3">{text}</span>
          <Cursor cursorColor="#F7AB0A" />
        </h1>
        <div className="pt-5">
          <Link href="#about">
            <button className="heroButton">About</button>
          </Link>
          <Link href="#experiences">
            <button className="heroButton">Experience</button>
          </Link>
          <Link href="#skills">
            <button className="heroButton">Skills</button>
          </Link>
          <Link href="#projects">
            <button className="heroButton">Projects</button>
          </Link>
          <Link href="#articles">
            <button className="heroButton">Articles</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
