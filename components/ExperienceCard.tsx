import Image from "next/image";
import React from "react";
import { motion } from "framer-motion";
import { Experience } from "@/typings";
import { urlFor } from "@/sanity";

type Props = {
  experience: Experience;
};

export default function ExperienceCard({ experience }: Props) {
  return (
    <article
      className="flex flex-col rounded-lg items-center space-y-7 flex-shrink-0 w-[80vw] md:w-[600px] 
    xl:w-[900px] snap-center bg-[#292929] p-10 hover:opacity-100 opacity-40 cursor-pointer transition-opacity duration-200 overflow-hidden"
    >
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{ duration: 1.2 }}
        whileInView={{ opacity: 1, y: 0 }}
        viewport={{ once: true }}
        src={urlFor(experience?.companyImage).url()}
        alt="pic"
        className=" h-24 w-24 object-cover xl:w-auto xl:h-48  object-center"
      />
      <div className="px-0 md:px-10">
        <h4 className="md:text-4xl  text-xl font-light">
          {experience.jobTitle}
        </h4>
        <p className="font-bold md:text-2xl  text-lg mt-1">
          {experience.company}
        </p>

        <div className="flex space-x-2 my-2 flex-wrap gap-2 ">
          {experience.technologies.map((skill) => (
            <div key={skill._id} className="relative rounded-full h-10 w-10 ">
              <Image
                src={urlFor(skill?.image).url()}
                fill
                alt="pic"
                className="rounded-full"
              />
            </div>
          ))}
        </div>
        <p className="uppercase py-5 text-gray-500">
          {new Date(experience.dateStarted).toDateString()} {"- "}
          {experience?.isCurrentlyWorkingHere ? (
            <span className="font-extrabold text-green-900">Present</span>
          ) : (
            new Date(experience.dateEnded).toDateString()
          )}
        </p>
        <ul className="list-disc space-y-4 ml-5 text-sm max-h-96 overflow-y-scroll scrollbar-thin scrollbar-track-black scrollbar-thumb-[#F7AB0A]/80 p-5">
          {experience.points.map((point, i) => (
            <li key={i}>{point}</li>
          ))}
        </ul>
      </div>
    </article>
  );
}
