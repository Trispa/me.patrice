"use client";
import React from "react";
import { motion } from "framer-motion";
import { LinkedInEmbed } from "react-social-media-embed";

function Articles() {
  const articles = [
    {
      url: "https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7156743890023170048",
    },
    {
      url: "https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7113522258945417216",
    },
  ];
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      className="h-screen flex relative overflow-hidden flex-col  text-left md:flex-row max-w-full px-10 justify-evenly mx-auto items-center"
    >
      <h3 className="md:absolute top-24 uppercase tracking-[20px] text-gray-500 md:text-2xl text-lg">
        Articles
      </h3>
      <div className="mt-5 h-[80%] w-full flex space-x-5 overflow-x-scroll p-10 snap-x snap-mandatory scrollbar-track-gray-400/20 scrollbar-thumb-[#F7AB0A]/80 scrollbar-thin">
        <div className="m-auto flex flex-wrap gap-10 w-full justify-center">
          {articles.map((article, index) => (
            <div className="min-w-48 w-[625px]" key={index}>
              <LinkedInEmbed url={article.url} width={"100%"} height={570} />
            </div>
          ))}
        </div>
      </div>
    </motion.div>
  );
}

export default Articles;
