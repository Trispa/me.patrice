/* eslint-disable @next/next/no-img-element */
"use client";
import React from "react";
import { motion } from "framer-motion";
import Project from "./Project";
import { Project as ProjectType } from "@/typings";

type Props = {
  projects: ProjectType[];
};

// export const projects = [1, 2, 3, 4, 5];
export default function Projects({ projects }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      className="h-screen relative flex overflow-hidden flex-cols text-left md:flex-row max-w-full justify-evenly mx-auto items-center z-0"
    >
      <h3 className="absolute top-24 uppercase tracking-[20px] text-gray-500 text-2xl">
        Projects
      </h3>
      <div className="relative w-full flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-20 scrollbar-track-gray-400/20 scrollbar-thumb-[#F7AB0A]/80 scrollbar-thin">
        {projects.map((project, i) => (
          <Project key={i} index={i} projects={projects} project={project} />
        ))}
      </div>
      <div className=" w-full absolute top-[30%] bg-[#F7AB0A]/20 left-0 h-[500px] -skew-y-12 " />
    </motion.div>
  );
}
