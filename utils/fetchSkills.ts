import { sanityClient, revalidate } from "@/sanity";
import { Skill } from "@/typings";
import { groq } from "next-sanity";

export const fetchSkills = async () => {
  const skills: Skill[] = await sanityClient.fetch(
    groq`
  *[_type=="skill"]
  `,
    { next: { revalidate } },
    { cache: "no-store" }
  );
  return skills;
};
