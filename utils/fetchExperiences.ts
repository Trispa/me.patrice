import { sanityClient, revalidate } from "@/sanity";
import { Experience } from "@/typings";
import { groq } from "next-sanity";

export const fetchExperiences = async () => {
  const experiences: Experience[] = await sanityClient.fetch(
    groq`
  *[_type=="experience"]{
    ...,
    technologies[]->,
  }
  `,
    { next: { revalidate } }
  );
  return experiences;
};
