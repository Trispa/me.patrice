import { sanityClient, revalidate } from "@/sanity";
import { Project } from "@/typings";
import { groq } from "next-sanity";

export const fetchProjects = async () => {
  const projects: Project[] = await sanityClient.fetch(
    groq`
  *[_type=="project"]{
    ...,
    technologies[]->
  }
  `,
    { next: { revalidate } }
  );
  return projects;
};
