import { sanityClient, revalidate } from "@/sanity";
import { Social } from "@/typings";
import { groq } from "next-sanity";

export const fetchSocials = async () => {
  const socials: Social[] = await sanityClient.fetch(
    groq`
  *[_type=="social"]
  `,
    { next: { revalidate } }
  );
  return socials;
};
