interface SanityBody {
  _createdAt: string;
  _id: string;
  _rew: string;
  _updatedAt: string;
}

interface Image {
  _type: "image";
  asset: {
    _ref: string;
    _type: "reference";
  };
}
export interface PageInfo extends SanityBody {
  _type: "pageInfo";
  email: string;
  address: string;
  name: string;
  heroImage: Image;
  profilePic: Image;
  phoneNumber: string;
  role: string;
  backgroundInformation: string;
}
export interface Social extends SanityBody {
  _type: "social";
  title: string;
  url: string;
}
export interface Project extends SanityBody {
  _type: "project";
  title: string;
  image: Image;
  linkToBuild: string;
  summary: string;
  technologies: skill[];
}
export interface Skill extends SanityBody {
  _type: "skill";
  title: string;
  image: Image;
  progress: number;
}
export interface Experience extends SanityBody {
  _type: "skill";
  title: string;
  companyImage: Image;
  progress: number;
  technologies: Skill[];
  dateStarted: string;
  dateEnded: string;
  isCurrentlyWorkingHere: boolean;
  points: string[];
  jobTitle: string;
  company: string;
}
